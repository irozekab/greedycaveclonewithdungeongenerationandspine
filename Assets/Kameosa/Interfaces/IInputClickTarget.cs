﻿using System;
using Kameosa.Cartesian;
using Kameosa.Components;

namespace Kameosa
{
    namespace Interfaces
    {
        public interface IInputClickTarget
        {
            #region Properties
            Coordinate Coordinate { get; }
            #endregion
        }
    }
}
