﻿using System;
using Kameosa.Components;

namespace Kameosa
{
    namespace Interfaces
    {
        public interface IDamageable
        {
            #region Actions
            event Action OnTakeDamage;
            event Action OnDie;
            #endregion

            #region Properties
            DamageableComponent DamageableComponent { get; }
            #endregion
        }
    }
}
