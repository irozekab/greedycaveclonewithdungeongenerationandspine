﻿using System;
using Kameosa.Components;

namespace Kameosa
{
    namespace Interfaces
    {
        public interface IGameplayEvents
        {
            #region Actions
            event Action OnGameStart;
            event Action OnGamePause;
            event Action OnGameResume;
            event Action OnGameWon;
            event Action OnGameLose;
            event Action OnGameEnd;
            event Action OnGameRestart;
            event Action OnGameQuit;
            #endregion
        }
    }
}
