﻿using UnityEngine;

namespace Kameosa
{
    namespace Controllers
    {
        [ExecuteInEditMode]
        public class CameraController : MonoBehaviour
        {
            #region Inspector Variables
            [SerializeField]
            private bool isSmoothing = true;
            [SerializeField]
            private float smoothing = 5f;
            [SerializeField]
            private UnityEngine.Vector3 offset = new UnityEngine.Vector3(0f, 15f, -22f);

            [Header("References")]
            [SerializeField]
            private Transform playerTransform;
            #endregion

            #region MonoBehaviour Functions
            private void Awake()
            {
                if (this.playerTransform == null)
                {
                    this.playerTransform = GameObject.Find("Player").transform;
                }
            }

            void Update()
            {
                UnityEngine.Vector3 targetCamPosition = this.playerTransform.position + this.offset;
                this.transform.position = this.isSmoothing ? UnityEngine.Vector3.Lerp(this.transform.position, targetCamPosition, this.smoothing * Time.deltaTime) : targetCamPosition;
            }
            #endregion
        }
    }
}

