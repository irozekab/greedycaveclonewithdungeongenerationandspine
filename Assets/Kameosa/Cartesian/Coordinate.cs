﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kameosa
{
    namespace Cartesian
    {
        [System.Serializable]
        public struct Coordinate 
        {
            #region Private Variables
            [SerializeField]
            private int x;
            [SerializeField]
            private int y;
            #endregion

            #region Static Properties
            public static Coordinate Up
            {
                get
                {
                    return new Coordinate(0, 1);
                }
            }

            public static Coordinate UpRight
            {
                get
                {
                    return new Coordinate(1, 1);
                }
            }

            public static Coordinate Right
            {
                get
                {
                    return new Coordinate(1, 0);
                }
            }

            public static Coordinate DownRight
            {
                get
                {
                    return new Coordinate(1, -1);
                }
            }

            public static Coordinate Down
            {
                get
                {
                    return new Coordinate(0, -1);
                }
            }

            public static Coordinate DownLeft
            {
                get
                {
                    return new Coordinate(-1, -1);
                }
            }

            public static Coordinate Left
            {
                get
                {
                    return new Coordinate(-1, 0);
                }
            }

            public static Coordinate UpLeft
            {
                get
                {
                    return new Coordinate(-1, 1);
                }
            }

            public static Coordinate North
            {
                get
                {
                    return Up;
                }
            }

            public static Coordinate NorthEast
            {
                get
                {
                    return UpRight;
                }
            }

            public static Coordinate East 
            {
                get
                {
                    return Right;
                }
            }

            public static Coordinate SouthEast 
            {
                get
                {
                    return DownRight;
                }
            }

            public static Coordinate South 
            {
                get
                {
                    return Down;
                }
            }

            public static Coordinate SouthWest
            {
                get
                {
                    return DownLeft;
                }
            }

            public static Coordinate West
            {
                get
                {
                    return Left;
                }
            }

            public static Coordinate NorthWest
            {
                get
                {
                    return UpLeft;
                }
            }

            public static List<Coordinate> CardinalDirections
            {
                get
                {
                    List<Coordinate> directions = new List<Coordinate>
                    {
                        Up,
                        Right,
                        Down,
                        Left
                    };

                    return directions;
                }
            }

            public static List<Coordinate> CardinalAndOrdinalDirections
            {
                get
                {
                    List<Coordinate> directions = new List<Coordinate>
                    {
                        Up,
                        UpRight,
                        Right,
                        DownRight,
                        Down,
                        DownLeft,
                        Left,
                        UpLeft
                    };

                    return directions;
                }
            }
            #endregion

            #region Properties
            public int X
            {
                get
                {
                    return this.x;
                }

                set
                {
                    this.x = value;
                }
            }

            public int Y
            {
                get
                {
                    return this.y;
                }

                set
                {
                    this.y = value;
                }
            }

            public int SqrMagnitude
            {
                get
                {
                    return (this.x * this.x) + (this.y * this.y);
                }
            }

            public Coordinate UpNeighbour
            {
                get
                {
                    return new Coordinate(this.x, this.y + 1);
                }
            }

            public Coordinate UpRightNeighbour
            {
                get
                {
                    return new Coordinate(this.x + 1, this.y + 1);
                }
            }

            public Coordinate RightNeighbour
            {
                get
                {
                    return new Coordinate(this.x + 1, this.y);
                }
            }

            public Coordinate DownRightNeighbour
            {
                get
                {
                    return new Coordinate(this.x + 1, this.y - 1);
                }
            }

            public Coordinate DownNeighbour
            {
                get
                {
                    return new Coordinate(this.x, this.y - 1);
                }
            }

            public Coordinate DownLeftNeighbour
            {
                get
                {
                    return new Coordinate(this.x - 1, this.y - 1);
                }
            }

            public Coordinate LeftNeighbour
            {
                get
                {
                    return new Coordinate(this.x - 1, this.y);
                }
            }

            public Coordinate UpLeftNeighbour
            {
                get
                {
                    return new Coordinate(this.x - 1, this.y + 1);
                }
            }

            public List<Coordinate> CardinalNeighbours
            {
                get
                {
                    List<Coordinate> neighbours = new List<Coordinate>();

                    foreach (Coordinate direction in Coordinate.CardinalDirections)
                    {
                        neighbours.Add(this + direction);
                    }

                    return neighbours;
                }
            }

            public List<Coordinate> CardinalAndOrdinalNeighbours
            {
                get
                {
                    List<Coordinate> neighbours = new List<Coordinate>();

                    foreach (Coordinate direction in Coordinate.CardinalAndOrdinalDirections)
                    {
                        neighbours.Add(this + direction);
                    }

                    return neighbours;
                }
            }
            #endregion

            public Coordinate(int x, int y)
            {
                this.x = x;
                this.y = y;
            }

            public Coordinate(UnityEngine.Vector2 vector2)
            {
                this.x = Mathf.RoundToInt(vector2.x);
                this.y = Mathf.RoundToInt(vector2.y);
            }

            public Coordinate(UnityEngine.Vector3 vector3)
            {
                this.x = Mathf.RoundToInt(vector3.x);
                this.y = Mathf.RoundToInt(vector3.y);
            }

            #region Operators
            public static implicit operator UnityEngine.Vector2(Coordinate coordinate)
            {
                return new UnityEngine.Vector2(coordinate.X, coordinate.Y);
            }

            public static implicit operator UnityEngine.Vector3(Coordinate coordinate)
            {
                return new UnityEngine.Vector3(coordinate.X, coordinate.Y, 0);
            }

            public static implicit operator Coordinate(UnityEngine.Vector2 vector2)
            {
                return new Coordinate((int)vector2.x, (int)vector2.y);
            }

            public static implicit operator string(Coordinate coordinate)
            {
                return "(" + coordinate.X + ", " + coordinate.Y + ")";
            }

            public static Coordinate operator +(Coordinate a, Coordinate b)
            {
                return new Coordinate(a.X + b.X, a.Y + b.Y);
            }

            public static Coordinate operator +(Coordinate a, UnityEngine.Vector2 b)
            {
                return new Coordinate(a.X + (int)b.x, a.Y + (int)b.y);
            }

            public static Coordinate operator -(Coordinate a, Coordinate b)
            {
                return new Coordinate(a.X - b.X, a.Y - b.Y);
            }

            public static Coordinate operator -(Coordinate a, UnityEngine.Vector2 b)
            {
                return new Coordinate(a.X - (int)b.x, a.Y - (int)b.y);
            }

            public static Coordinate operator *(Coordinate a, int b)
            {
                return new Coordinate(a.X * b, a.Y * b);
            }
            //public static implicit operator UnityEngine.Vector2(Coordinate coordinate)
            //{
            //    return new UnityEngine.Vector2(coordinate.X, coordinate.Y);
            //}

            //public static implicit operator UnityEngine.Vector3(Coordinate coordinate)
            //{
            //    return new UnityEngine.Vector3(coordinate.X, coordinate.Y, 0);
            //}

            //public static implicit operator Coordinate(UnityEngine.Vector2 vector2)
            //{
            //    return new Coordinate((int)vector2.x, (int)vector2.y);
            //}

            //public static implicit operator Coordinate(UnityEngine.Vector3 vector3)
            //{
            //    return new Coordinate((int)vector3.x, (int)vector3.y);
            //}

            //public static implicit operator string(Coordinate coordinate)
            //{
            //    return "(" + coordinate.X + ", " + coordinate.Y + ")";
            //}

            //public static Coordinate operator +(Coordinate a, Coordinate b)
            //{
            //    return new Coordinate(a.X + b.X, a.Y + b.Y);
            //}

            //public static Coordinate operator +(Coordinate a, UnityEngine.Vector2 b)
            //{
            //    return new Coordinate(a.X + (int)b.x, a.Y + (int)b.y);
            //}

            //public static Coordinate operator +(Coordinate a, UnityEngine.Vector3 b)
            //{
            //    return new Coordinate(a.X + (int)b.x, a.Y + (int)b.y);
            //}

            //public static Coordinate operator +(UnityEngine.Vector2 a, Coordinate b)
            //{
            //    return new Coordinate((int)a.x + b.X, (int)a.y + b.Y);
            //}

            //public static Coordinate operator +(UnityEngine.Vector3 a, Coordinate b)
            //{
            //    return new Coordinate((int)a.x + b.X, (int)a.y + b.Y);
            //}

            //public static Coordinate operator -(Coordinate a, Coordinate b)
            //{
            //    return new Coordinate(a.X - b.X, a.Y - b.Y);
            //}

            //public static Coordinate operator -(Coordinate a, UnityEngine.Vector2 b)
            //{
            //    return new Coordinate(a.X - (int)b.x, a.Y - (int)b.y);
            //}

            //public static Coordinate operator -(UnityEngine.Vector2 a, Coordinate b)
            //{
            //    return new Coordinate((int)a.x - b.X, (int)a.y - b.Y);
            //}

            //public static Coordinate operator -(Coordinate a, UnityEngine.Vector3 b)
            //{
            //    return new Coordinate(a.X - (int)b.x, a.Y - (int)b.y);
            //}

            //public static Coordinate operator -(UnityEngine.Vector3 a, Coordinate b)
            //{
            //    return new Coordinate((int)a.x - b.X, (int)a.y - b.Y);
            //}

            //public static Coordinate operator *(Coordinate a, int b)
            //{
            //    return new Coordinate(a.X * b, a.Y * b);
            //}
            #endregion

            #region Public Functions
            public bool IsAbove(Coordinate other)
            {
                return this.x > other.X;
            }

            public bool IsBelow(Coordinate other)
            {
                return this.x < other.X;
            }

            public bool IsRightOf(Coordinate other)
            {
                return this.y > other.Y;
            }

            public bool IsLeftOf(Coordinate other)
            {
                return this.y < other.Y;
            }

            public bool Equals(Coordinate other)
            {
                return (this.x == other.X) && (this.y == other.Y);
            }

            public bool NotEquals(Coordinate other)
            {
                return !Equals(other);
            }

            public override int GetHashCode()
            {
                return this.x ^ this.y;
            }

            public int GetManhattanDistanceTo(Coordinate other)
            {
                int xDistance = Mathf.Abs(this.x - other.X);
                int yDistance = Mathf.Abs(this.y - other.Y);

                return yDistance + xDistance;
            }

            public override string ToString()
            {
                return string.Format("({0}, {1})", this.x, this.y);
            }
            #endregion

            #region Public Static Functions
            public static Coordinate GetRandomCardinalDirection()
            {
                int random = UnityEngine.Random.Range(0, 4);
                Coordinate result = Up;

                switch (random)
                {
                    case 0:
                        result = Up;
                        break;
                    case 1:
                        result = Right;
                        break;
                    case 2:
                        result = Down;
                        break;
                    case 3:
                        result = Left;
                        break;
                }

                return result;
            }
            #endregion
        }
    }
}
