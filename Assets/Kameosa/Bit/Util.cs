﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Kameosa
{
   namespace Bit
    {
        public static class Util 
        {
            public static int GetSetBits(int n)
            {
                int result = 0;

                while (n != 0)
                {
                    n &= n - 1;
                    result++;
                }

                return result;
            }

            public static bool IsSetBit(int n, int i)
            {
                return ((n >> i) & 1) == 1;
            }
        }
    }
}
