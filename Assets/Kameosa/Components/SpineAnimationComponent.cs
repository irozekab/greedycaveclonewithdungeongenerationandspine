﻿using System.Collections;
using System.Collections.Generic;
using Kameosa.Components;
using Kameosa.Interfaces;
using Spine.Unity;
using UnityEngine;

namespace Kameosa
{
    namespace Components
    {
        public class SpineAnimationComponent : GameplayEventEnableComponent
        {
            #region Inspector Variables
            [Header("Animation")]
            [SerializeField]
            private string idleAnimationName = "Idle";
            //[SerializeField]
            //private string walkAnimationName = "Walk";
            [SerializeField]
            private string runAnimationName = "Run";
            [SerializeField]
            private string attackAnimationName = "Attack";
            [Space(10)]

            [Header("References")]
            [SerializeField]
            private SkeletonAnimation frontSkeletonAnimation;
            [SerializeField]
            private SkeletonAnimation backSkeletonAnimation;
            [SerializeField]
            private SkeletonAnimation sideSkeletonAnimation;
            #endregion

            #region Private Variables
            private SkeletonAnimation currentSkeletonAnimation;
            #endregion

            #region MonoBehaviour Functions
            protected override void Start()
            {
                base.Start();

                this.currentSkeletonAnimation = this.frontSkeletonAnimation;
            }
            #endregion

            #region Public Functions
            public void PlayRun(UnityEngine.Vector3 velocity)
            {
                SetCurrentSkeletonAnimation(velocity);
                this.currentSkeletonAnimation.AnimationName = this.runAnimationName;
            }

            public void PlayIdle()
            {
                this.currentSkeletonAnimation.AnimationName = this.idleAnimationName;
            }

            public void PlayAttack()
            {
                this.currentSkeletonAnimation.AnimationName = this.attackAnimationName;
            }
            #endregion

            #region Private Functions
            private void SetCurrentSkeletonAnimation(UnityEngine.Vector3 velocity)
            {
                if (velocity.x != 0f)
                {
                    bool isFacingLeft = velocity.x < 0f;
                    this.sideSkeletonAnimation.Skeleton.FlipX = isFacingLeft;

                    this.currentSkeletonAnimation = this.sideSkeletonAnimation;
                }
                else if (velocity.y > 0f)
                {
                    this.currentSkeletonAnimation = this.backSkeletonAnimation;
                }
                else
                {
                    this.currentSkeletonAnimation = this.frontSkeletonAnimation;
                }

                if (!this.currentSkeletonAnimation.gameObject.activeInHierarchy)
                {
                    this.frontSkeletonAnimation.gameObject.SetActive(this.currentSkeletonAnimation == this.frontSkeletonAnimation);
                    this.backSkeletonAnimation.gameObject.SetActive(this.currentSkeletonAnimation == this.backSkeletonAnimation);
                    this.sideSkeletonAnimation.gameObject.SetActive(this.currentSkeletonAnimation == this.sideSkeletonAnimation);
                }
            }

#if UNITY_EDITOR
            private void DebugCurrentSkeletonAnimation()
            {
                //http://esotericsoftware.com/spine-api-reference#Skeleton

                Debug.Log("this.currentSkeletonAnimation: " + this.currentSkeletonAnimation.name);

                // All animation names.
                foreach (Spine.Animation animation in this.currentSkeletonAnimation.Skeleton.Data.Animations)
                {
                    Debug.Log(animation.Name);
                }

                // Current animation name.
                Debug.Log(this.currentSkeletonAnimation.AnimationName);
            }
#endif
            #endregion
        }
    }
}
