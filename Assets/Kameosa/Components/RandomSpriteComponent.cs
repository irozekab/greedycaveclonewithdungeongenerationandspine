﻿using System.Collections;
using System.Collections.Generic;
using Kameosa.Collections.IList;
using UnityEngine;

namespace Kameosa
{
    namespace Components
    {
        [ExecuteInEditMode]
        [RequireComponent(typeof(SpriteRenderer))]
        public class RandomSpriteComponent : MonoBehaviour
        {
            #region Inspector Variables
            [SerializeField]
            private List<Sprite> sprites;
            #endregion

            #region Private Variables
            private SpriteRenderer spriteRenderer;
            #endregion

            #region MonoBehaviour Functions
            private void Awake()
            {
                this.spriteRenderer = GetComponent<SpriteRenderer>();
            }

            private void Start()
            {
                if (this.sprites != null && this.sprites.Count > 0)
                {
                    this.spriteRenderer.sprite = this.sprites.GetRandom();
                }
            }
            #endregion
        }
    }
}
