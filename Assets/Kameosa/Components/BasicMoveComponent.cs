﻿using Kameosa.Enumerables;
using UnityEngine;

namespace Kameosa
{
    namespace Components
    {
        [RequireComponent(typeof(Rigidbody))]
        [RequireComponent(typeof(SpineAnimationComponent))]
        public class BasicMoveComponent : GameplayEventEnableComponent
        {
            #region Inspector Variables
            [SerializeField]
            protected float maxVelocity = 5f;
            [SerializeField]
            protected bool isNormalizingInput = true;

            [Header("References")]
            [SerializeField]
            protected SpineAnimationComponent spineAnimationComponent;
            #endregion

            #region Private Variables
            protected new Rigidbody rigidbody;
            protected MoveState state = MoveState.Idle;
            #endregion

            #region Properties
            public MoveState State
            {
                get
                {
                    return this.state;
                }

                set
                {
                    this.state = value;
                }
            }
            #endregion

            #region MonoBehaviour Functions
            protected override void Awake()
            {
                base.Awake();

                this.rigidbody = GetComponent<Rigidbody>();
                this.spineAnimationComponent = GetComponent<SpineAnimationComponent>();
            }
            #endregion

            #region Public Functions
            public virtual void TryMove(UnityEngine.Vector3 velocity)
            {
                if (this.isNormalizingInput)
                {
                    velocity = velocity.normalized;
                }

                if (velocity.magnitude > 0f)
                {
                    this.spineAnimationComponent.PlayRun(velocity);
                    this.rigidbody.MovePosition(this.rigidbody.position + (velocity * this.maxVelocity * Time.fixedDeltaTime));
                    this.state = MoveState.Run;
                }
                else
                {
                    TryIdle();
                }
            }

            public virtual void TryIdle()
            {
                if (this.state != MoveState.Idle)
                {
                    this.spineAnimationComponent.PlayIdle();
                    this.state = MoveState.Idle;
                }
            }

            public virtual void TryAttack()
            {
                if (this.state != MoveState.Attack)
                {
                    this.spineAnimationComponent.PlayAttack();
                    this.state = MoveState.Attack;
                }
            }
            #endregion
        }
    }
}
