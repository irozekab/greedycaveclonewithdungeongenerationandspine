﻿using System;
using System.Collections;
using System.Collections.Generic;
using Kameosa.Cartesian;
using Kameosa.Components;
using Kameosa.Interfaces;
using UnityEngine;

namespace Kameosa
{
    namespace Components
    {
        [RequireComponent(typeof(MoveComponent))]
        public class BasicInputComponent : GameplayEventEnableComponent
        {
            #region Inspector Variables
            [Header("References")]
            [SerializeField]
            private BasicMoveComponent basicMoveComponent;
            #endregion

            #region MonoBehaviour Functions
            protected override void Awake()
            {
                base.Awake();

                this.basicMoveComponent = GetComponent<BasicMoveComponent>();
            }

            protected virtual void Update()
            {
                UnityEngine.Vector3 input = new UnityEngine.Vector3(Input.GetAxisRaw(Kameosa.Constants.Input.HORIZONTAL), Input.GetAxisRaw(Kameosa.Constants.Input.VERTICAL), 0f);
                HandleInput(input);
            }
            #endregion

            #region Protected Functions
            protected virtual void HandleInput(UnityEngine.Vector3 input)
            {
                this.basicMoveComponent.TryMove(input);
            }
            #endregion
        }
    }
}
