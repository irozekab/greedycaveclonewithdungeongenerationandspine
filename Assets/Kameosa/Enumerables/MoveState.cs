﻿namespace Kameosa
{
    namespace Enumerables
    {
        public enum MoveState
        {
            Idle,
            Walk,
            Run,
            Attack
        };
    }
}
