﻿using System.Collections.Generic;
using System.Diagnostics;
using Kameosa.Cartesian;
using Kameosa.Collections.Heap;
using Kameosa.Components;
using Kameosa.Enumerables;
using UnityEngine;

public class MoveComponent : BasicMoveComponent
{
    private const int OPEN_SET_MAX_COUNT = 100;

    #region Inspector Variables
    [Header("References")]
    [SerializeField]
    private DungeonController dungeon;
    [SerializeField]
    private Tile currentTile;
    #endregion

    #region Private Variables
    private Coroutine currentCoroutine;
    #endregion

    #region Properties
    public Tile CurrentTile
    {
        get
        {
            return this.currentTile;
        }

        set
        {
            this.currentTile = value;
        }
    }
    #endregion

    #region MonoBehaviour Functions
    protected override void Awake()
    {
        base.Awake();

        if (this.dungeon == null)
        {
            this.dungeon = GameObject.Find("Dungeon").GetComponent<DungeonController>();
        }
    }

    protected override void Start()
    {
        base.Start();

        if (this.dungeon == null)
        {
            UnityEngine.Debug.Log("1");
        }

        if (this.dungeon.Dungeon == null)
        {
            UnityEngine.Debug.Log("2");
        }


        if (this.rigidbody == null)
        {
            UnityEngine.Debug.Log("3");
        }



        this.currentTile = this.dungeon.Dungeon.Tiles[Mathf.RoundToInt(this.rigidbody.position.x), Mathf.RoundToInt(this.rigidbody.position.y)];
    }
    #endregion

    #region Public Functions
    public override void TryIdle()
    {
        base.TryIdle();

        if (this.currentCoroutine != null)
        {
            StopCoroutine(this.currentCoroutine);
        }
    }

    public void TryMoveTo(Coordinate destinationCoordinate)
    {
        this.state = MoveState.Run;
        Stack<Tile> path = GetPathTo(destinationCoordinate);
        this.currentCoroutine = StartCoroutine(MoveSmoothly(path));
    }
    #endregion

    #region Private Functions
    private Stack<Tile> GetPathTo(Coordinate endCoordinate)
    {
#if UNITY_EDITOR
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();
#endif

        Tile endTile = this.dungeon.Dungeon.Tiles[endCoordinate.X, endCoordinate.Y];
        Heap<Tile> openSet = new Heap<Tile>(OPEN_SET_MAX_COUNT);
        HashSet<Tile> closedSet = new HashSet<Tile>();

        openSet.Add(this.currentTile);

        while (openSet.Count > 0)
        {
            Tile currentOpenSetFirstTile = openSet.RemoveFirst();
            closedSet.Add(currentOpenSetFirstTile);

            if (currentOpenSetFirstTile.Coordinate == endCoordinate)
            {
                break;
            }

            List<Coordinate> neighbourCoordinates = currentOpenSetFirstTile.Coordinate.CardinalNeighbours;

            foreach (Coordinate neighbourCoordinate in neighbourCoordinates)
            {
                Tile neighbourTile = this.dungeon.Dungeon.Tiles[neighbourCoordinate.X, neighbourCoordinate.Y];
                if (neighbourTile.Type != TileType.Hollow || closedSet.Contains(neighbourTile))
                {
                    continue;
                }

                int newMovementCostToNeightbourTile = currentOpenSetFirstTile.GCost + currentOpenSetFirstTile.GetManhattanDistanceTo(neighbourTile);

                if (newMovementCostToNeightbourTile < neighbourTile.GCost || !openSet.Contains(neighbourTile))
                {
                    neighbourTile.GCost = newMovementCostToNeightbourTile;
                    neighbourTile.HCost = neighbourTile.GetManhattanDistanceTo(endTile);
                    neighbourTile.PreviousTile = currentOpenSetFirstTile;

                    if (!openSet.Contains(neighbourTile))
                    {
                        openSet.Add(neighbourTile);
                    }
                    else
                    {
                        openSet.UpdateNode(neighbourTile);
                    }
                }
            }
        }

        Stack<Tile> path = new Stack<Tile>();
        Tile currentPathTile = endTile;

        while (currentPathTile != this.currentTile)
        {
            path.Push(currentPathTile);
            currentPathTile = currentPathTile.PreviousTile;
        }

#if UNITY_EDITOR
        stopwatch.Stop();

        UnityEngine.Debug.Log(string.Format("<color=red>dungeonPathfindingComponent.GetPath()</color> Total: {0} ms.", stopwatch.ElapsedMilliseconds));
#endif

        return path;
    }

    System.Collections.IEnumerator MoveSmoothly(Stack<Tile> path)
    {
        while (path.Count > 0)
        {
            Tile nextTile = path.Pop();
            Coordinate direction = nextTile.Coordinate - this.currentTile.Coordinate;
            float sqrRemainingDistance = (this.transform.position - nextTile.Coordinate).sqrMagnitude;

            this.spineAnimationComponent.PlayRun(direction);

            while (sqrRemainingDistance > float.Epsilon)
            {
                Vector3 newPosition = Vector3.MoveTowards(this.transform.position, nextTile.Coordinate, this.maxVelocity * Time.deltaTime);
                this.rigidbody.MovePosition(newPosition);
                sqrRemainingDistance = (this.transform.position - nextTile.Coordinate).sqrMagnitude;

                yield return null;
            }

            this.currentTile = nextTile;
        }

        TryIdle();
    }
    #endregion
}
