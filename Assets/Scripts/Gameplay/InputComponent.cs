﻿using System.Collections;
using System.Collections.Generic;
using Kameosa.Cartesian;
using Kameosa.Components;
using Kameosa.Enumerables;
using Kameosa.Interfaces;
using UnityEngine;

[RequireComponent(typeof(MoveComponent))]
public class InputComponent : GameplayEventEnableComponent
{
    #region Inspector Variables
    [Header("References")]
    [SerializeField]
    private MoveComponent moveComponent;
    #endregion

    #region MonoBehaviour Functions
    protected override void Awake()
    {
        base.Awake();

        this.moveComponent = GetComponent<MoveComponent>();
    }

    protected virtual void Update()
    {
        if (Input.GetMouseButtonDown(Kameosa.Constants.Input.MOUSE_LEFT))
        {
            Ray ray = Camera.main.ScreenPointToRay(UnityEngine.Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);

            if (hit)
            {
                HandleMouseLeftClick(hit.transform.GetComponent<IInputClickTarget>());
            }
        }
    }
    #endregion

    #region Private Functions
    private void HandleMouseLeftClick(IInputClickTarget clickTarget)
    {
        if (this.moveComponent.State == MoveState.Idle)
        {
            this.moveComponent.TryMoveTo(clickTarget.Coordinate);
        }
        else if (this.moveComponent.State == MoveState.Run || this.moveComponent.State == MoveState.Walk)
        {
            this.moveComponent.TryIdle();
        }
    }
    #endregion
}
