﻿using Kameosa.Cartesian;
using Kameosa.Collections.IList;
using Kameosa.Services;
using Kameosa.Enumerables;
using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using System;
using System.Text.RegularExpressions;

/**
 * Random Tunnel Length Drunkard Walk Algorithm
 */
[RequireComponent(typeof(DungeonInstantiationComponent))]
public class DungeonGenerationComponent : MonoBehaviour
{
    private const int UP_BIT_CONFIGURATION_VALUE = 1;
    private const int UPRIGHT_BIT_CONFIGURATION_VALUE = 2;
    private const int RIGHT_BIT_CONFIGURATION_VALUE = 4;
    private const int DOWNRIGHT_BIT_CONFIGURATION_VALUE = 8;
    private const int DOWN_BIT_CONFIGURATION_VALUE = 16;
    private const int DOWNLEFT_BIT_CONFIGURATION_VALUE = 32;
    private const int LEFT_BIT_CONFIGURATION_VALUE = 64;
    private const int UPLEFT_BIT_CONFIGURATION_VALUE = 128;
    private const int BIT_CONFIGURATION_VALUE_SUM = 255;

    #region Inspector Variables
    [Header("Generation Parameters")]
    [SerializeField]
    private bool isGenerateNewRandomSeedEachTime = false;
    [SerializeField]
    private int randomSeed = 0;
    [SerializeField]
    [Range(5, 50)]
    private int width = 50;
    [SerializeField]
    [Range(5, 50)]
    private int height = 50;
    [SerializeField]
    [Range(5, 300)]
    private int tunnelCount = 50;
    [SerializeField]
    [Range(2, 10)]
    private int maxTunnelLength = 8;
    [Space(10)]
    #endregion

    #region Private Variables
    private Dungeon dungeon;
    private List<Tile> secondPassWallTiles;
    private List<Tile> thirdPassWallTiles;
    private Queue<Tile> fourthPassWallTiles;
    #endregion

    #region Public Functions
    // https://medium.freecodecamp.org/how-to-make-your-own-procedural-dungeon-map-generator-using-the-random-walk-algorithm-e0085c8aa9a
    public Dungeon Generate()
    {
        if (this.isGenerateNewRandomSeedEachTime)
        {
            this.randomSeed = (int) System.DateTime.Now.Ticks;
        }

        Debug.Log("this.randomSeed: " + this.randomSeed);
        UnityEngine.Random.InitState(this.randomSeed);

        this.dungeon = new Dungeon(this.width, this.height);
        this.secondPassWallTiles = new List<Tile>();
        this.thirdPassWallTiles = new List<Tile>();
        this.fourthPassWallTiles = new Queue<Tile>();

        StartDrunkenWalk();
        SetBasicWalls();
        SetSpecificWallTypes();

        return this.dungeon;
    }
    #endregion

    #region Private Functions
    private void StartDrunkenWalk()
    {
        int tunnelLeft = this.tunnelCount;
        Coordinate lastDirection = Coordinate.GetRandomCardinalDirection();
        Coordinate lastCoordinate = new Coordinate(this.width / 2, this.height / 2);
        this.dungeon.Tiles[lastCoordinate.X, lastCoordinate.Y].Type = TileType.Hollow;
        this.dungeon.HollowTiles.Add(this.dungeon.Tiles[lastCoordinate.X, lastCoordinate.Y]);

        while (tunnelLeft > 0)
        {
            lastDirection = GetNextDrunkenDirection(lastCoordinate, lastDirection);
            lastCoordinate = DigRandomTunnelLength(lastCoordinate, lastDirection);

            tunnelLeft--;
        }
    }

    private Coordinate GetNextDrunkenDirection(Coordinate lastCoordinate, Coordinate lastDirection)
    {
        Coordinate nextDirection;

        do
        {
            nextDirection = Coordinate.GetRandomCardinalDirection();
        } while (nextDirection == lastDirection || IsCoordinateOutOfBoundForHollowTileTypes(lastCoordinate + nextDirection));

        return nextDirection;
    }

    private Coordinate DigRandomTunnelLength(Coordinate currentCoordinate, Coordinate direction)
    {
        int length = UnityEngine.Random.Range(1, this.maxTunnelLength + 1);

        while (length > 0 && !IsCoordinateOutOfBoundForHollowTileTypes(currentCoordinate + direction))
        {
            currentCoordinate += direction;
            this.dungeon.Tiles[currentCoordinate.X, currentCoordinate.Y].Type = TileType.Hollow;
            this.dungeon.HollowTiles.Add(this.dungeon.Tiles[currentCoordinate.X, currentCoordinate.Y]);
            length--;
        }

        return currentCoordinate;
    }

    private void SetBasicWalls()
    {
        foreach (Tile tile in this.dungeon.HollowTiles)
        {
            List<Coordinate> neighbours = tile.Coordinate.CardinalAndOrdinalNeighbours;

            foreach (Coordinate neighbour in neighbours)
            {
                if (IsCoordinateOfTileType(neighbour, TileType.Solid))
                {
                    this.dungeon.Tiles[neighbour.X, neighbour.Y].Type = TileType.Wall;
                    this.dungeon.WallTiles.Add(this.dungeon.Tiles[neighbour.X, neighbour.Y]);
                }
            }
        }
    }

    private void SetSpecificWallTypes()
    {
        SetWallTileConfiguration();
        SetWallTileTypeFirstPass();
        SetWallTileTypeSecondPass();
        SetWallTileTypeThirdPass();
        SetWallTileTypeFourthPass();
    }

    private void SetWallTileConfiguration()
    {
        foreach (Tile tile in this.dungeon.WallTiles)
        {
            int configuration = BIT_CONFIGURATION_VALUE_SUM;
            int temp = UP_BIT_CONFIGURATION_VALUE;
            List<Coordinate> neighbours = tile.Coordinate.CardinalAndOrdinalNeighbours;

            foreach (Coordinate neighbour in neighbours)
            {
                if (IsCoordinateOutOfBound(neighbour) ||
                    IsCoordinateOfTileType(neighbour, TileType.Hollow) ||
                    IsCoordinateOfTileType(neighbour, TileType.Solid))
                {
                    configuration -= temp;
                }

                temp <<= 1;
            }

            tile.Configuration = configuration;
        }
    }

    /**
     * Set the most obvious tile types.
     */
    private void SetWallTileTypeFirstPass()
    {
        foreach (Tile tile in this.dungeon.WallTiles)
        {
            TileType tileType = TileType.Unknown;
            string binaryConfiguration = Convert.ToString(tile.Configuration, 2).PadLeft(8, '0');
            int cardinalNeighbourWallsCount = GetCardinalNeighbourWallsCount(tile.Configuration);

            if (cardinalNeighbourWallsCount == 0)
            {
                /**
                 * Island tiles
                 */
                if (Regex.IsMatch(binaryConfiguration, ".0.0.0.0"))
                {
                    tileType = TileType.Wall_0;
                }
            }
            else if (cardinalNeighbourWallsCount == 1)
            {
                /**
                 * Horizontal & vertical end tiles
                 */
                if (Regex.IsMatch(binaryConfiguration, ".0.1.0.0"))
                {
                    tileType = TileType.Wall_16;
                }
                else if (Regex.IsMatch(binaryConfiguration, ".1.0.0.0"))
                {
                    tileType = TileType.Wall_64;
                }
                if (Regex.IsMatch(binaryConfiguration, ".0.0.0.1"))
                {
                    tileType = TileType.Wall_1;
                }
                else if (Regex.IsMatch(binaryConfiguration, ".0.0.1.0"))
                {
                    tileType = TileType.Wall_4;
                }
            }
            else if (cardinalNeighbourWallsCount == 2)
            {
                /**
                 * Horizontal & vertical tiles
                 */
                if (Regex.IsMatch(binaryConfiguration, "...1...1"))
                {
                    tileType = TileType.Wall_17;
                }
                else if (Regex.IsMatch(binaryConfiguration, ".1...1.."))
                {
                    tileType = TileType.Wall_68;
                }

                /**
                 * Corner tiles
                 */
                if (Regex.IsMatch(binaryConfiguration, ".1.1.0.0"))
                {
                    tileType = TileType.Wall_80;
                }
                else if (Regex.IsMatch(binaryConfiguration, ".1.0.0.1"))
                {
                    tileType = TileType.Wall_65;
                }
                else if (Regex.IsMatch(binaryConfiguration, ".0.0.1.1"))
                {
                    tileType = TileType.Wall_5;
                }
                else if (Regex.IsMatch(binaryConfiguration, ".0.1.1.0"))
                {
                    tileType = TileType.Wall_20;
                }
            }
            else if (cardinalNeighbourWallsCount < 5)
            {
                if (Regex.IsMatch(binaryConfiguration, ".11111.0") ||
                    Regex.IsMatch(binaryConfiguration, "11.0.111"))
                {
                    tileType = TileType.Wall_68;
                }
                else if (Regex.IsMatch(binaryConfiguration, ".0.11111") ||
                         Regex.IsMatch(binaryConfiguration, "1111.0.1"))
                {
                    tileType = TileType.Wall_17;
                }
            }

            if (tileType == TileType.Unknown)
            {
                this.secondPassWallTiles.Add(tile);
            }
            
            tile.Type = tileType;
        }
    }

    /**
     * Set the missed corner tile types.
     */
    private void SetWallTileTypeSecondPass()
    {
        foreach (Tile tile in this.secondPassWallTiles)
        {
            TileType tileType = TileType.Unknown;
            string binaryConfiguration = Convert.ToString(tile.Configuration, 2).PadLeft(8, '0');

            if (Regex.IsMatch(binaryConfiguration, ".101...."))
            {
                Coordinate downLeftNeighbourCoordinate = tile.Coordinate.DownLeftNeighbour;
                TileType downLeftNeighbourTileType = IsCoordinateOutOfBound(downLeftNeighbourCoordinate) ? TileType.Unknown : this.dungeon.Tiles[downLeftNeighbourCoordinate.X, downLeftNeighbourCoordinate.Y].Type;

                if (downLeftNeighbourTileType == TileType.Hollow)
                {
                    tileType = TileType.Wall_80;
                }
            }
            else if (Regex.IsMatch(binaryConfiguration, "01.....1"))
            {
                Coordinate upLeftNeighbourCoordinate = tile.Coordinate.UpLeftNeighbour;
                TileType upLeftNeighbourTileType = IsCoordinateOutOfBound(upLeftNeighbourCoordinate) ? TileType.Unknown : this.dungeon.Tiles[upLeftNeighbourCoordinate.X, upLeftNeighbourCoordinate.Y].Type;

                if (upLeftNeighbourTileType == TileType.Hollow)
                {
                    tileType = TileType.Wall_65;
                }
            }
            else if (Regex.IsMatch(binaryConfiguration, ".....101"))
            {
                Coordinate upRightNeighbourCoordinate = tile.Coordinate.UpRightNeighbour;
                TileType upRightNeighbourTileType = IsCoordinateOutOfBound(upRightNeighbourCoordinate) ? TileType.Unknown : this.dungeon.Tiles[upRightNeighbourCoordinate.X, upRightNeighbourCoordinate.Y].Type;

                if (upRightNeighbourTileType == TileType.Hollow)
                {
                    tileType = TileType.Wall_5;
                }
            }
            else if (Regex.IsMatch(binaryConfiguration, "...101.."))
            {
                Coordinate downRightNeighbourCoordinate = tile.Coordinate.DownRightNeighbour;
                TileType downRightNeighbourTileType = IsCoordinateOutOfBound(downRightNeighbourCoordinate) ? TileType.Unknown : this.dungeon.Tiles[downRightNeighbourCoordinate.X, downRightNeighbourCoordinate.Y].Type;

                if (downRightNeighbourTileType == TileType.Hollow)
                {
                    tileType = TileType.Wall_20;
                }
            }

            if (tileType == TileType.Unknown)
            {
                this.thirdPassWallTiles.Add(tile);
            }
            else
            {
                this.fourthPassWallTiles.Enqueue(tile);
                tile.Type = tileType;
            }
        }
    }

    /**
     * Use neighbour tile types to decide the best tile type of current tile.
     */
    private void SetWallTileTypeThirdPass()
    {
        foreach (Tile tile in this.thirdPassWallTiles)
        {
            int cardinalNeighboursConfiguration = 0;
            TileType tileType = TileType.Unknown;

            Coordinate upNeighbourCoordinate = tile.Coordinate.UpNeighbour;
            TileType upNeighbourTileType = IsCoordinateOutOfBound(upNeighbourCoordinate) ? TileType.Unknown : this.dungeon.Tiles[upNeighbourCoordinate.X, upNeighbourCoordinate.Y].Type;
            if (IsOfEndWallTileType(upNeighbourTileType) || upNeighbourTileType == TileType.Wall_17 || upNeighbourTileType == TileType.Wall_80 || upNeighbourTileType == TileType.Wall_20)
            {
                cardinalNeighboursConfiguration += UP_BIT_CONFIGURATION_VALUE;
            }

            Coordinate rightNeighbourCoordinate = tile.Coordinate.RightNeighbour;
            TileType rightNeighbourTileType = IsCoordinateOutOfBound(rightNeighbourCoordinate) ? TileType.Unknown : this.dungeon.Tiles[rightNeighbourCoordinate.X, rightNeighbourCoordinate.Y].Type;
            if (IsOfEndWallTileType(rightNeighbourTileType) || rightNeighbourTileType == TileType.Wall_68 || rightNeighbourTileType == TileType.Wall_65 || rightNeighbourTileType == TileType.Wall_80)
            {
                cardinalNeighboursConfiguration += RIGHT_BIT_CONFIGURATION_VALUE;
            }

            Coordinate downNeighbourCoordinate = tile.Coordinate.DownNeighbour;
            TileType downNeighbourTileType = IsCoordinateOutOfBound(downNeighbourCoordinate) ? TileType.Unknown : this.dungeon.Tiles[downNeighbourCoordinate.X, downNeighbourCoordinate.Y].Type;
            if (IsOfEndWallTileType(downNeighbourTileType) || downNeighbourTileType == TileType.Wall_17 || downNeighbourTileType == TileType.Wall_65 || downNeighbourTileType == TileType.Wall_5)
            {
                cardinalNeighboursConfiguration += DOWN_BIT_CONFIGURATION_VALUE;
            }

            Coordinate leftNeighbourCoordinate = tile.Coordinate.LeftNeighbour;
            TileType leftNeighbourTileType = IsCoordinateOutOfBound(leftNeighbourCoordinate) ? TileType.Unknown : this.dungeon.Tiles[leftNeighbourCoordinate.X, leftNeighbourCoordinate.Y].Type;
            if (IsOfEndWallTileType(leftNeighbourTileType) || leftNeighbourTileType == TileType.Wall_68 || leftNeighbourTileType == TileType.Wall_20 || leftNeighbourTileType == TileType.Wall_5)
            {
                cardinalNeighboursConfiguration += LEFT_BIT_CONFIGURATION_VALUE;
            }

            if (upNeighbourTileType == TileType.Unknown || rightNeighbourTileType == TileType.Unknown || downNeighbourTileType == TileType.Unknown || leftNeighbourTileType == TileType.Unknown)
            {
                cardinalNeighboursConfiguration = 999;
            }

            switch (cardinalNeighboursConfiguration)
            {
                case 5:
                    tileType = TileType.Wall_5;
                    break;

                case 17:
                    tileType = TileType.Wall_17;
                    break;

                case 20:
                    tileType = TileType.Wall_20;
                    break;

                case 21:
                    tileType = TileType.Wall_21;
                    break;

                case 65:
                    tileType = TileType.Wall_65;
                    break;

                case 68:
                    tileType = TileType.Wall_68;
                    break;

                case 69:
                    tileType = TileType.Wall_69;
                    break;

                case 80:
                    tileType = TileType.Wall_80;
                    break;

                case 81:
                    tileType = TileType.Wall_81;
                    break;

                case 84:
                    tileType = TileType.Wall_84;
                    break;

                case 85:
                    tileType = TileType.Wall_85;
                    break;
            }

            tile.Type = tileType;
        }
    }

    private void SetWallTileTypeFourthPass()
    {
        while (this.fourthPassWallTiles.Count > 0)
        {
            Tile tile = this.fourthPassWallTiles.Dequeue();

            if (IsDisconnectedWithUpNeighbour(tile))
            {
                SetTileTypeToConnectWithUpNeightbour(tile);
                this.fourthPassWallTiles.Enqueue(tile);
            }
            else if (IsDisconnectedWithRightNeighbour(tile))
            {
                SetTileTypeToConnectWithRightNeightbour(tile);
                this.fourthPassWallTiles.Enqueue(tile);
            }
            else if (IsDisconnectedWithDownNeighbour(tile))
            {
                SetTileTypeToConnectWithDownNeightbour(tile);
                this.fourthPassWallTiles.Enqueue(tile);
            }
            else if (IsDisconnectedWithLeftNeighbour(tile))
            {
                SetTileTypeToConnectWithLeftNeightbour(tile);
                this.fourthPassWallTiles.Enqueue(tile);
            }
        }
    }

    private int GetCardinalNeighbourWallsCount(int configuration)
    {
        int result = 0;

        for (int i = 0; i < 8; i += 2)
        {
            if (((configuration >> i) & 1) == 1)
            {
                result++;
            }
        }

        return result;
    }

    private int GetCardinalAndOrdinalNeighbourWallsCount(int configuration)
    {
        int result = 0;

        for (int i = 0; i < 8; i += 1)
        {
            if (((configuration >> i) & 1) == 1)
            {
                result++;
            }
        }

        return result;
    }

    private bool IsCoordinateOutOfBoundForHollowTileTypes(Coordinate coordinate)
    {
        return coordinate.X < 1 || coordinate.Y < 1 || coordinate.X >= (this.dungeon.Width - 1) || coordinate.Y >= (this.dungeon.Height - 1);
    }

    private bool IsCoordinateOutOfBound(Coordinate coordinate)
    {
        return coordinate.X < 0 || coordinate.Y < 0 || coordinate.X >= this.dungeon.Width || coordinate.Y >= this.dungeon.Height;
    }

    private bool IsCoordinateOfTileType(Coordinate coordinate, TileType tileType)
    {
        if (IsCoordinateOutOfBound(coordinate))
        {
            return false;
        }

        return this.dungeon.Tiles[coordinate.X, coordinate.Y].Type == tileType;
    }

    private bool IsOfEndWallTileType(TileType tileType)
    {
        return tileType == TileType.Wall_1 || tileType == TileType.Wall_4 || tileType == TileType.Wall_16 || tileType == TileType.Wall_64;
    }

    private bool IsDisconnectedWithUpNeighbour(Tile tile)
    {
        TileType tileType = tile.Type;
        Coordinate upNeighbourCoordinate = tile.Coordinate.UpNeighbour;
        TileType upNeighbourTileType = IsCoordinateOutOfBound(upNeighbourCoordinate) ? TileType.Unknown : this.dungeon.Tiles[upNeighbourCoordinate.X, upNeighbourCoordinate.Y].Type;
        bool isConnectedTo = upNeighbourTileType == TileType.Wall_16 || 
                             upNeighbourTileType == TileType.Wall_17 || 
                             upNeighbourTileType == TileType.Wall_20 || 
                             upNeighbourTileType == TileType.Wall_21 || 
                             upNeighbourTileType == TileType.Wall_80 || 
                             upNeighbourTileType == TileType.Wall_81 || 
                             upNeighbourTileType == TileType.Wall_84 || 
                             upNeighbourTileType == TileType.Wall_85;

        if (tileType == TileType.Wall_1 || 
            tileType == TileType.Wall_5 || 
            tileType == TileType.Wall_17 || 
            tileType == TileType.Wall_21 || 
            tileType == TileType.Wall_65 || 
            tileType == TileType.Wall_69 || 
            tileType == TileType.Wall_81 || 
            tileType == TileType.Wall_85)
        {
            return !isConnectedTo;
        }

        return isConnectedTo;
    }

    private bool IsDisconnectedWithRightNeighbour(Tile tile)
    {
        TileType tileType = tile.Type;
        Coordinate rightNeighbourCoordinate = tile.Coordinate.RightNeighbour;
        TileType rightNeighbourTileType = IsCoordinateOutOfBound(rightNeighbourCoordinate) ? TileType.Unknown : this.dungeon.Tiles[rightNeighbourCoordinate.X, rightNeighbourCoordinate.Y].Type;
        bool isConnectedTo = rightNeighbourTileType == TileType.Wall_64 || 
                             rightNeighbourTileType == TileType.Wall_65 || 
                             rightNeighbourTileType == TileType.Wall_68 || 
                             rightNeighbourTileType == TileType.Wall_69 || 
                             rightNeighbourTileType == TileType.Wall_80 || 
                             rightNeighbourTileType == TileType.Wall_81 || 
                             rightNeighbourTileType == TileType.Wall_84 || 
                             rightNeighbourTileType == TileType.Wall_85;

        if (tileType == TileType.Wall_4 || 
            tileType == TileType.Wall_5 || 
            tileType == TileType.Wall_20 || 
            tileType == TileType.Wall_21 || 
            tileType == TileType.Wall_68 || 
            tileType == TileType.Wall_69 || 
            tileType == TileType.Wall_84 || 
            tileType == TileType.Wall_85)
        {
            return !isConnectedTo;
        }

        return isConnectedTo;
    }

    private bool IsDisconnectedWithDownNeighbour(Tile tile)
    {
        TileType tileType = tile.Type;
        Coordinate downNeighbourCoordinate = tile.Coordinate.DownNeighbour;
        TileType downNeighbourTileType = IsCoordinateOutOfBound(downNeighbourCoordinate) ? TileType.Unknown : this.dungeon.Tiles[downNeighbourCoordinate.X, downNeighbourCoordinate.Y].Type;
        bool isConnectedTo = downNeighbourTileType == TileType.Wall_1 || 
                             downNeighbourTileType == TileType.Wall_5 || 
                             downNeighbourTileType == TileType.Wall_17 || 
                             downNeighbourTileType == TileType.Wall_21 || 
                             downNeighbourTileType == TileType.Wall_65 || 
                             downNeighbourTileType == TileType.Wall_69 || 
                             downNeighbourTileType == TileType.Wall_81 || 
                             downNeighbourTileType == TileType.Wall_85;

        if (tileType == TileType.Wall_16 || 
            tileType == TileType.Wall_17 || 
            tileType == TileType.Wall_20 || 
            tileType == TileType.Wall_21 || 
            tileType == TileType.Wall_80 || 
            tileType == TileType.Wall_81 || 
            tileType == TileType.Wall_84 || 
            tileType == TileType.Wall_85)
        {
            return !isConnectedTo;
        }

        return isConnectedTo;
    }

    private bool IsDisconnectedWithLeftNeighbour(Tile tile)
    {
        TileType tileType = tile.Type;
        Coordinate leftNeighbourCoordinate = tile.Coordinate.LeftNeighbour;
        TileType leftNeighbourTileType = IsCoordinateOutOfBound(leftNeighbourCoordinate) ? TileType.Unknown : this.dungeon.Tiles[leftNeighbourCoordinate.X, leftNeighbourCoordinate.Y].Type;
        bool isConnectedTo = leftNeighbourTileType == TileType.Wall_4 || 
                             leftNeighbourTileType == TileType.Wall_5 || 
                             leftNeighbourTileType == TileType.Wall_20 || 
                             leftNeighbourTileType == TileType.Wall_21 || 
                             leftNeighbourTileType == TileType.Wall_68 || 
                             leftNeighbourTileType == TileType.Wall_69 || 
                             leftNeighbourTileType == TileType.Wall_84 || 
                             leftNeighbourTileType == TileType.Wall_85;

        if (tileType == TileType.Wall_64 || 
            tileType == TileType.Wall_65 || 
            tileType == TileType.Wall_68 || 
            tileType == TileType.Wall_69 || 
            tileType == TileType.Wall_80 || 
            tileType == TileType.Wall_81 || 
            tileType == TileType.Wall_84 || 
            tileType == TileType.Wall_85)
        {
            return !isConnectedTo;
        }

        return isConnectedTo;
    }

    private void SetTileTypeToConnectWithUpNeightbour(Tile tile)
    {
        TileType tileType = tile.Type;

        switch (tileType)
        {
            case TileType.Wall_0:
                tileType = TileType.Wall_1;
                break;

            case TileType.Wall_4:
                tileType = TileType.Wall_5;
                break;

            case TileType.Wall_16:
                tileType = TileType.Wall_17;
                break;

            case TileType.Wall_20:
                tileType = TileType.Wall_21;
                break;

            case TileType.Wall_64:
                tileType = TileType.Wall_65;
                break;

            case TileType.Wall_68:
                tileType = TileType.Wall_69;
                break;

            case TileType.Wall_80:
                tileType = TileType.Wall_81;
                break;

            case TileType.Wall_84:
                tileType = TileType.Wall_85;
                break;
        }

        tile.Type = tileType;
    }

    private void SetTileTypeToConnectWithRightNeightbour(Tile tile)
    {
        TileType tileType = tile.Type;

        switch (tileType)
        {
            case TileType.Wall_0:
                tileType = TileType.Wall_4;
                break;

            case TileType.Wall_1:
                tileType = TileType.Wall_5;
                break;

            case TileType.Wall_16:
                tileType = TileType.Wall_20;
                break;

            case TileType.Wall_17:
                tileType = TileType.Wall_21;
                break;

            case TileType.Wall_64:
                tileType = TileType.Wall_68;
                break;

            case TileType.Wall_65:
                tileType = TileType.Wall_69;
                break;

            case TileType.Wall_80:
                tileType = TileType.Wall_84;
                break;

            case TileType.Wall_81:
                tileType = TileType.Wall_85;
                break;
        }

        tile.Type = tileType;
    }

    private void SetTileTypeToConnectWithDownNeightbour(Tile tile)
    {
        TileType tileType = tile.Type;

        switch (tileType)
        {
            case TileType.Wall_0:
                tileType = TileType.Wall_16;
                break;

            case TileType.Wall_1:
                tileType = TileType.Wall_17;
                break;

            case TileType.Wall_4:
                tileType = TileType.Wall_20;
                break;

            case TileType.Wall_5:
                tileType = TileType.Wall_21;
                break;

            case TileType.Wall_64:
                tileType = TileType.Wall_80;
                break;

            case TileType.Wall_65:
                tileType = TileType.Wall_81;
                break;

            case TileType.Wall_68:
                tileType = TileType.Wall_84;
                break;

            case TileType.Wall_69:
                tileType = TileType.Wall_85;
                break;
        }

        tile.Type = tileType;
    }

    private void SetTileTypeToConnectWithLeftNeightbour(Tile tile)
    {
        TileType tileType = tile.Type;

        switch (tileType)
        {
            case TileType.Wall_0:
                tileType = TileType.Wall_64;
                break;

            case TileType.Wall_1:
                tileType = TileType.Wall_65;
                break;

            case TileType.Wall_4:
                tileType = TileType.Wall_68;
                break;

            case TileType.Wall_5:
                tileType = TileType.Wall_69;
                break;

            case TileType.Wall_16:
                tileType = TileType.Wall_80;
                break;

            case TileType.Wall_17:
                tileType = TileType.Wall_81;
                break;

            case TileType.Wall_20:
                tileType = TileType.Wall_84;
                break;

            case TileType.Wall_21:
                tileType = TileType.Wall_85;
                break;
        }

        tile.Type = tileType;
    }
    #endregion
}