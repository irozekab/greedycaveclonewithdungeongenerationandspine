﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(DungeonGenerationComponent))]
public class DungeonGenerationComponentEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        DungeonGenerationComponent dungeonGenerationComponent = this.target as DungeonGenerationComponent;

        //if (DrawDefaultInspector())
        //{
        //    dungeonGenerationComponent.Generate();
        //}

        if (GUILayout.Button("Regenerate"))
        {
            dungeonGenerationComponent.Generate();
        }
    }
}
