﻿using System.Collections;
using System.Collections.Generic;
using Kameosa.Cartesian;
using UnityEngine;

public class DungeonInstantiationComponent : MonoBehaviour 
{
    #region Inspector Variables
    [Header("Tiles")]
    [SerializeField]
    private GameObject dirtTilePrefab;
    [SerializeField]
    private GameObject solidTilePrefab;

    [SerializeField]
    private GameObject WallTilePrefab_0;
    [SerializeField]
    private GameObject WallTilePrefab_1;
    [SerializeField]
    private GameObject WallTilePrefab_4;
    [SerializeField]
    private GameObject WallTilePrefab_5;
    [SerializeField]
    private GameObject WallTilePrefab_16;
    [SerializeField]
    private GameObject WallTilePrefab_17;
    [SerializeField]
    private GameObject WallTilePrefab_20;
    [SerializeField]
    private GameObject WallTilePrefab_21;
    [SerializeField]
    private GameObject WallTilePrefab_64;
    [SerializeField]
    private GameObject WallTilePrefab_65;
    [SerializeField]
    private GameObject WallTilePrefab_68;
    [SerializeField]
    private GameObject WallTilePrefab_69;
    [SerializeField]
    private GameObject WallTilePrefab_80;
    [SerializeField]
    private GameObject WallTilePrefab_81;
    [SerializeField]
    private GameObject WallTilePrefab_84;
    [SerializeField]
    private GameObject WallTilePrefab_85;
    [Space(10)]
    #endregion

    #region Private Variables
    private string wrapperName = "Wrapper";
    private Transform wrapper;
    #endregion

    //#region Properties
    //public GameObject DirtTilePrefab
    //{
    //    get
    //    {
    //        return this.dirtTilePrefab;
    //    }

    //    set
    //    {
    //        this.dirtTilePrefab = value;
    //    }
    //}
    //#endregion

    #region MonoBehavior Functions
    private void Start() 
    {
    }

    private void Update() 
    {
    }
    #endregion

    #region Public Functions
    public void Instantiate(Dungeon dungeon)
    {
        Reset();

        Transform hollowTilesWrapper = new GameObject("Hollow Tiles").transform;
        hollowTilesWrapper.SetParent(this.wrapper);

        Transform wallTilesWrapper = new GameObject("Wall Tiles").transform;
        wallTilesWrapper.SetParent(this.wrapper);

        foreach (Tile tile in dungeon.HollowTiles)
        {
            GameObject gameObject = Instantiate(GetPrefabForTileType(tile.Type), tile.Coordinate, Quaternion.identity).gameObject;
            gameObject.GetComponent<TileController>().Tile = tile;
            gameObject.transform.SetParent(hollowTilesWrapper);
        }

        foreach (Tile tile in dungeon.WallTiles)
        {
            GameObject gameObject = Instantiate(GetPrefabForTileType(tile.Type), tile.Coordinate, Quaternion.identity).gameObject;
            gameObject.GetComponent<TileController>().Tile = tile;
            gameObject.transform.SetParent(wallTilesWrapper);
        }
    }
    #endregion

    #region Private Functions
    private void Reset()
    {
        if (this.transform.Find(this.wrapperName))
        {
            DestroyImmediate(this.transform.Find(this.wrapperName).gameObject);
        }

        this.wrapper = new GameObject(this.wrapperName).transform;
        this.wrapper.SetParent(this.transform);
    }

    private GameObject GetPrefabForTileType(TileType tileType)
    {
        GameObject prefab = null;

        switch (tileType)
        {
            case TileType.Hollow:
                prefab = this.dirtTilePrefab;
                break;

            case TileType.Wall_0:
                prefab = this.WallTilePrefab_0;
                break;

            case TileType.Wall_1:
                prefab = this.WallTilePrefab_1;
                break;

            case TileType.Wall_4:
                prefab = this.WallTilePrefab_4;
                break;

            case TileType.Wall_5:
                prefab = this.WallTilePrefab_5;
                break;

            case TileType.Wall_16:
                prefab = this.WallTilePrefab_16;
                break;

            case TileType.Wall_17:
                prefab = this.WallTilePrefab_17;
                break;

            case TileType.Wall_20:
                prefab = this.WallTilePrefab_20;
                break;

            case TileType.Wall_21:
                prefab = this.WallTilePrefab_21;
                break;

            case TileType.Wall_64:
                prefab = this.WallTilePrefab_64;
                break;

            case TileType.Wall_65:
                prefab = this.WallTilePrefab_65;
                break;

            case TileType.Wall_68:
                prefab = this.WallTilePrefab_68;
                break;

            case TileType.Wall_69:
                prefab = this.WallTilePrefab_69;
                break;

            case TileType.Wall_80:
                prefab = this.WallTilePrefab_80;
                break;

            case TileType.Wall_81:
                prefab = this.WallTilePrefab_81;
                break;

            case TileType.Wall_84:
                prefab = this.WallTilePrefab_84;
                break;

            case TileType.Wall_85:
                prefab = this.WallTilePrefab_85;
                break;

            default:
                Debug.LogWarning("Trying to instantiate a tile type that isn't implemented in DungeonInstantiationComponent. (" + tileType + ").");
                break;
        }

        return prefab;
    }
    #endregion
}
