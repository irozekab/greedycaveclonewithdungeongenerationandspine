﻿using System.Collections;
using System.Collections.Generic;
using Kameosa.Cartesian;
using Kameosa.Interfaces;
using UnityEngine;

public class TileController : MonoBehaviour, IInputClickTarget 
{
    #region Inspector Variables
    [SerializeField]
    private Tile tile;
    #endregion

    #region Private Variables
    #endregion

    #region Properties
    public Tile Tile
    {
        get
        {
            return this.tile;
        }

        set
        {
            this.tile = value;
        }
    }

    public Coordinate Coordinate
    {
        get
        {
            return this.tile.Coordinate;
        }
    }
    #endregion

    #region MonoBehavior Functions
    protected virtual void Start() 
    {
    }

    protected virtual void Update() 
    {
    }
    #endregion

    #region Public Functions
    #endregion

    #region Private Functions
    #endregion
}
