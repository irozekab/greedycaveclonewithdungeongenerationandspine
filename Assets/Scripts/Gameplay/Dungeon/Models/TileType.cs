﻿public enum TileType
{
    Unknown,
    Solid,
    Hollow,

    /* The number represents a sum that tells us the configuration of neighbour walls to the current wall.
     * The 8 directional positions are represented with incremental binary values like so:
     *
     *  128 1   2
     *  64      4
     *  32  16  8
     *
     * For example, a wall always start with a configuration of 0. If there's a wall to it's right, 
     * the configuration will now be 4. If there's also a wall to it's down, its configuration now becomes 20.
     */
    Wall,
    Wall_0,
    Wall_1,
    Wall_4,
    Wall_5,
    Wall_16,
    Wall_17,
    Wall_20,
    Wall_21,
    Wall_64,
    Wall_65,
    Wall_68,
    Wall_69,
    Wall_80,
    Wall_81,
    Wall_84,
    Wall_85,
}