﻿using Kameosa.Cartesian;
using Kameosa.Collections.Heap;
using UnityEngine;

[System.Serializable]
public class Tile : IHeapNode<Tile>
{
    #region Inspector Variables
    [SerializeField]
    private int x = -1;
    [SerializeField]
    private int y = -1;
    [SerializeField]
    private int configuration = 0;
    [SerializeField]
    private TileType type = TileType.Solid;
    #endregion

    #region Private Variables
    private int heapIndex;
    private int gCost;
    private int hCost;
    private Tile previousTile;
    #endregion

    #region Properties
    public int X
    {
        get
        {
            return this.x;
        }

        set
        {
            this.x = value;
        }
    }

    public int Y
    {
        get
        {
            return this.y;
        }

        set
        {
            this.y = value;
        }
    }

    public Coordinate Coordinate
    {
        get
        {
            return new Coordinate(this.x, this.y);
        }
    }

    public int Configuration
    {
        get
        {
            return this.configuration;
        }

        set
        {
            this.configuration = value;
        }
    }

    public TileType Type
    {
        get
        {
            return this.type;
        }

        set
        {
            this.type = value;
        }
    }

    public int HeapIndex
    {
        get
        {
            return this.heapIndex;
        }

        set
        {
            this.heapIndex = value;
        }
    }

    // Sum of gCost and hCost.
    public int FCost
    {
        get
        {
            return this.gCost + this.hCost;
        }
    }

    // Cost from start tile to current tile.
    public int GCost
    {
        get
        {
            return this.gCost;
        }

        set
        {
            this.gCost = value;
        }
    }

    // Cost from current tile to end tile.
    public int HCost
    {
        get
        {
            return this.hCost;
        }

        set
        {
            this.hCost = value;
        }
    }

    // Best previous tile of current.
    public Tile PreviousTile
    {
        get
        {
            return this.previousTile;
        }

        set
        {
            this.previousTile = value;
        }
    }
    #endregion

    public Tile(int x, int y, TileType type)
    {
        this.x = x;
        this.y = y;
        this.type = type;
    }

    #region Public Functions
    public int CompareTo(Tile other)
    {
        int compare = FCost.CompareTo(other.FCost);

        if (compare == 0)
        {
            compare = this.hCost.CompareTo(other.HCost);
        }

        return -compare;
    }

    public int GetManhattanDistanceTo(Tile other)
    {
        return this.Coordinate.GetManhattanDistanceTo(other.Coordinate);
    }
    #endregion
}