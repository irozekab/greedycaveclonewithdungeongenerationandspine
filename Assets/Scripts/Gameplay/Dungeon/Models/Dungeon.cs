﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Kameosa.Cartesian;
using Kameosa.Collections.Heap;
using UnityEngine;

public class Dungeon : IEnumerable
{
    private const int OPEN_SET_MAX_COUNT = 100;

    #region Private Variables
    private Tile[,] tiles;
    private List<Tile> hollowTiles;
    private List<Tile> wallTiles;
    #endregion

    #region properties
    public int Width
    {
        get
        {
            return this.tiles.GetLength(0);
        }
    }

    public int Height
    {
        get
        {
            return this.tiles.GetLength(1);
        }
    }

    public Tile[,] Tiles
    {
        get
        {
            return this.tiles;
        }
    }

    public List<Tile> HollowTiles
    {
        get
        {
            return this.hollowTiles;
        }
    }

    public List<Tile> WallTiles
    {
        get
        {
            return this.wallTiles;
        }
    }
    #endregion

    #region Public Functions
    public Dungeon(int width = 20, int height = 20)
    {
        this.tiles = new Tile[width, height];
        this.hollowTiles = new List<Tile>();
        this.wallTiles = new List<Tile>();

        for (int x = 0; x < this.tiles.GetLength(0); x++)
        {
            for (int y = 0; y < this.tiles.GetLength(1); y++)
            {
                this.tiles[x, y] = new Tile(x, y, TileType.Solid);
            }
        }
    }

    public Stack<Coordinate> GetPath(Coordinate startCoordinate, Coordinate endCoordinate)
    {
#if UNITY_EDITOR
        Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();
#endif

        Tile startTile = this.tiles[startCoordinate.X, startCoordinate.Y];
        Tile endTile = this.tiles[endCoordinate.X, endCoordinate.Y];
        Heap<Tile> openSet = new Heap<Tile>(OPEN_SET_MAX_COUNT);
        HashSet<Tile> closedSet = new HashSet<Tile>();

        openSet.Add(this.tiles[startCoordinate.X, startCoordinate.Y]);

        while (openSet.Count > 0)
        {
            Tile currentTile = openSet.RemoveFirst();
            closedSet.Add(currentTile);

            if (currentTile.Coordinate == endCoordinate)
            {
                break;
            }

            List<Coordinate> neighbourCoordinates = currentTile.Coordinate.CardinalNeighbours;

            foreach (Coordinate neighbourCoordinate in neighbourCoordinates)
            {
                Tile neighbourTile = this.tiles[neighbourCoordinate.X, neighbourCoordinate.Y];
                if (neighbourTile.Type != TileType.Hollow || closedSet.Contains(neighbourTile))
                {
                    continue;
                }

                int newMovementCostToNeightbourTile = currentTile.GCost + currentTile.GetManhattanDistanceTo(neighbourTile);

                if (newMovementCostToNeightbourTile < neighbourTile.GCost || !openSet.Contains(neighbourTile))
                {
                    neighbourTile.GCost = newMovementCostToNeightbourTile;
                    neighbourTile.HCost = neighbourTile.GetManhattanDistanceTo(endTile);
                    neighbourTile.PreviousTile = currentTile;

                    if (!openSet.Contains(neighbourTile))
                    {
                        openSet.Add(neighbourTile);
                    }
                    else
                    {
                        openSet.UpdateNode(neighbourTile);
                    }
                }
            }
        }

        Stack<Coordinate> path = new Stack<Coordinate>();
        Tile currentPathTile = endTile;

        while (currentPathTile != startTile)
        {
            path.Push(currentPathTile.Coordinate - currentPathTile.PreviousTile.Coordinate);
            currentPathTile = currentPathTile.PreviousTile;
        }

#if UNITY_EDITOR
        stopwatch.Stop();

        UnityEngine.Debug.Log(string.Format("<color=red>dungeon.GetPath()</color> Total: {0} ms.", stopwatch.ElapsedMilliseconds));
#endif

        return path;
    }

    public IEnumerator GetEnumerator()
    {
        for (int x = 0; x < this.tiles.GetLength(0); x++)
        {
            for (int y = 0; y < this.tiles.GetLength(1); y++)
            {
                yield return this.tiles[x, y];
            }
        }
    }
    #endregion
}