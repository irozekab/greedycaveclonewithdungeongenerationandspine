﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonController : MonoBehaviour 
{
    private const float GIZMO_TILE_HIGHLIGHT_BORDER_SIZE = 0.1f;
    private const float GIZMO_TILE_SIZE = 0.4f;

    #region Inspector Variables
#if UNITY_EDITOR
    [Header("Gizmos")]
    [SerializeField]
    private bool isShowGizmo = true;
    [SerializeField]
    private bool isShowLabel = true;
    [SerializeField]
    private bool isShowHighlightedGizmo = true;
    [SerializeField]
    private TileType highlightedTileType = TileType.Hollow;
    [Space(10)]
#endif

    [Header("References")]
    [SerializeField]
    private DungeonGenerationComponent dungeonGenerationComponent;
    [SerializeField]
    private DungeonInstantiationComponent dungeonInstantiationComponent;
    [Space(10)]
    #endregion

    #region Private Variables
    private Dungeon dungeon;
    #endregion

    #region Properties
    public Dungeon Dungeon
    {
        get
        {
            return this.dungeon;
        }

        set
        {
            this.dungeon = value;
        }
    }
    #endregion

    #region MonoBehavior Functions
    private void Awake()
    {
        if (this.dungeonGenerationComponent == null)
        {
            this.dungeonGenerationComponent = GetComponent<DungeonGenerationComponent>();
        }

        if (this.dungeonInstantiationComponent == null)
        {
            this.dungeonInstantiationComponent = GetComponent<DungeonInstantiationComponent>();
        }

        this.dungeon = this.dungeonGenerationComponent.Generate();
    }

    private void Start() 
    {
        this.dungeonInstantiationComponent.Instantiate(this.dungeon);
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if ((this.dungeon != null) && this.isShowGizmo)
        {
            for (int x = 0; x < this.dungeon.Width; x++)
            {
                for (int y = 0; y < this.dungeon.Height; y++)
                {
                    if (this.isShowHighlightedGizmo && this.dungeon.Tiles[x, y].Type == this.highlightedTileType)
                    {
                        UnityEngine.Gizmos.color = Color.red;
                        UnityEngine.Gizmos.DrawCube(new Vector3(x, y), UnityEngine.Vector3.one * (GIZMO_TILE_SIZE + GIZMO_TILE_HIGHLIGHT_BORDER_SIZE));
                    }

                    GUIStyle style = new GUIStyle
                    {
                        alignment = TextAnchor.MiddleCenter
                    };

                    switch (this.dungeon.Tiles[x, y].Type)
                    {
                        case TileType.Hollow:
                            style.normal.textColor = Color.white;
                            UnityEngine.Gizmos.color = Color.white;
                            break;

                        case TileType.Solid:
                            style.normal.textColor = Color.green;
                            UnityEngine.Gizmos.color = Color.green;
                            break;

                        default:
                            style.normal.textColor = Color.black;
                            UnityEngine.Gizmos.color = Color.black;
                            break;
                    }

                    if (this.isShowLabel)
                    {
                        UnityEditor.Handles.Label(new Vector3(x, y), (this.dungeon.Tiles[x, y].Configuration).ToString(), style);
                    }
                    else
                    {
                        UnityEngine.Gizmos.DrawCube(new Vector3(x, y), UnityEngine.Vector3.one * GIZMO_TILE_SIZE);
                    }
                }
            }
        }
    }
#endif
    #endregion

    #region Public Functions
    #endregion

    #region Private Functions
    #endregion
}
