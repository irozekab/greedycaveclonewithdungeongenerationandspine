# GreedyCaveCloneWithDungeonGenerationAndSpine

### What is this repository for?

GreedyCaveCloneWithDungeonGenerationAndSpine started as a Ludem Dare game jam entry which I failed to submit. I have carried on working on it and will slowly turn it into a 2.5D platformer.

### What is the current state?

The Kameosa library is v0.4.

- 2nd newest.
- Just keep for future reference.

### How do I get set up?

1. Download Unity and run Unity.
2. Update project to latest Unity version if necessary.
3. Fix compilation errors which might show up in future Unity version.
4. Start coding.